/*

        Документация:
        
        https://developer.mozilla.org/ru/docs/HTML/HTML5/Constraint_validation
        
        form.checkValidity() > Проверка всех полей формы на валидость
        form.reportValidity() > Проверяет все поля на валидность и выводит возле каждого из не прошедшего валидацию
            сообщение с ошибкой

        formElement.validity > Объект с параметрами валидности поля 
        formElement.setCustomValidity(message) > Метод который задаст validity.valid = false, и при попытке отправки
            сообщения выведет message в браузерный попал

        Классы для стилизации состояния элемента
        input:valid{}
        input:invalid{}

        
        Задание:
        
        Используя браузерное API для валидации форм реализовать валидацию следующей формы:
        

        - Имя пользователя: type:text -> validation: required; minlength = 2;  
            Если пустое выввести сообщение: "Как тебя зовут дружище?!"
        - Email: type: email -> validation: required; minlength = 3; validEmail;
            Если эмейл не валидный вывести сообщение "Ну и зря, не получишь бандероль с яблоками!"
        - Пароль: type: password -> validation: required; minlength = 8; maxlength=16;
            Если пустой вывести сообщение: "Я никому не скажу наш секрет";
        - Количество сьеденых яблок: type: number -> validation: required; minlength = 1; validNumber;
            Если количество 0 вывести эррор с сообщением "Ну хоть покушай немного... Яблочки вкусные"
        - Напиши спасибо за яблоки: type: text -> validation: required; 
            Если текст !== "спасибо" вывести эррор с сообщением "Фу, неблагодарный(-ая)!" используя setCustomValidity();

        - Согласен на обучение: type: checkbox -> validation: required;
            Если не выбран вывести эррор с сообщение: "Необразованные живут дольше! Хорошо подумай!"

        Внизу две кнопки:

        1) Обычный submit который отправит форму, если она валидна.
        2) Кнопка Validate(Проверить) которая запускает методы:
            - yourForm.checkValidity: и выводит на страницу сообщение с результатом проверки
            - yourForm.reportValidity: вызывает проверку всех правил и вывод сообщения с ошибкой, если такая есть

    */


document.addEventListener('DOMContentLoaded', function(){
    let myValidateForm=document.forms.myValidateForm;
    let userName=myValidateForm.elements.userName;
    let mail=myValidateForm.elements.mail;
    let pass=myValidateForm.elements.pass;
    let apple=myValidateForm.elements.apple;
    let thanks=myValidateForm.elements.thanks;
    let agreement=myValidateForm.elements.agreement;
    let submit=myValidateForm.elements.submit;
    let checkValidation=myValidateForm.elements.validateBtn;
    

    function validateForm(){
        if (userName.value==='') {
            userName.setCustomValidity('Как тебя зовут дружище?!')
        } else { userName.setCustomValidity('')
    };   
        let reg='/\b\w*)@(\w+\.\w+\b/';
        if (!mail.value(reg)){
            mail.setCustomValidity("Ну и зря, не получишь бандероль с яблоками!")
        } 
        else { mail.setCustomValidity('')
    };
        if (pass.length<=7) {
            pass.setCustomValidity('Я никому не скажу наш секрет')
        } else{ pass.setCustomValidity('')
    }
       if(apple.value<=0){
            apple.setCustomValidity('Ну хоть покушай немного... Яблочки вкусные')
        } else{apple.setCustomValidity('')
    }
        if (thanks.value!="спасибо"){
            thanks.setCustomValidity("Фу, неблагодарный(-ая)!")
        };
       if (!agreement.checked) {
            agreement.setCustomValidity("Необразованные живут дольше! Хорошо подумай!")
        };
    }
    checkValidation.addEventListener('click', function(){
        validateForm();
       if(myValidateForm.checkValidity()){
           myValidateForm.classList.add('valid');
           document.write('Success!');
       } else{
        myValidateForm.classList.add('invalid');
        document.write('Error!')
       };  
    });
    submit.addEventListener('click', function(){
        if(!validateForm) {
            submit.disabled='true';
        }
    })
})


