var myButton1 = document.querySelector('button[data-tab="1"]');
var myButton2 = document.querySelector('button[data-tab="2"]');
var myButton3 = document.querySelector('button[data-tab="3"]');
var allImg = document.querySelectorAll('.tab');

function hideAllTabs() {
  document.querySelectorAll('.active').forEach(function(allImg) {
allImg.classList.remove('active')
  }) };
  



myButton1.onclick = function(event) {
  hideAllTabs();
  var img1 = document.querySelector('div.tab[data-tab="1"]');
  img1.classList.add("active");
  
};

myButton2.onclick = function(event) {
  hideAllTabs();
  var img2 = document.querySelector('div.tab[data-tab="2"]');
  img2.classList.add("active");
};

myButton3.onclick = function(event) {
  hideAllTabs();
  var img3 = document.querySelector('div.tab[data-tab="3"]');
  img3.classList.add("active");
};





  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
